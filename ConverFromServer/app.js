﻿﻿//http://localhost/BaseAPI/input/post.json?node=P0002&json={P1:200,P2:20,P3:100,P4:50}&time=1452182852
//http://localhost/BaseAPI/input/list.json
//17085_I: {name: "LIGHTING - I", res: 900, field: "I", units: "A"}
//17085_V: { name: "LIGHTING - V", res: 900, field: "V", units: "V"}
//http://localhost/BaseAPI/input/post.json?node=P0002&json={P1:200,P2:20,P3:100,P4:50}&time=1452182852
//http://localhost/BaseAPI/input/set.json?inputid=36&fields={%22description%22:%22GRET%22}

//First step 
//http://localhost/BaseAPI/input/post.json?node=LIGHTING&json={current:0,voltage:0}
//http://localhost/BaseAPI/input/post.json?node=AC-Paula&json={current:0,voltage:0}
//http://localhost/BaseAPI/input/post.json?node=AC-Main-office&json={current:0,voltage:0}
//http://localhost/BaseAPI/input/post.json?node=AC-Tim-Ve&json={current:0,voltage:0}
//http://localhost/BaseAPI/input/post.json?node=AC-Meeting&json={current:0,voltage:0}
//Step 2 
//Call take data from http://eniscope.com/ajax/readings?res=60&fields%5B%5D=V&fields%5B%5D=I&meter=17085&dateType=custom&from=2016-01-07&to=2016-01-07&phaseType=1
//LIGHTING 17085
//AC Paula 17081
//AC Meeting 17078
//AC Main office  17079
//AC Tim 17080
var http = require('http');
var request = require('request-json');
var httpSync;
try {
    httpSync = require('http-sync');
} catch (ex) {
    httpSync = require('http-sync-win');
}

var Sequence = exports.Sequence || require('sequence').Sequence
    , sequence = Sequence.create()
    , err
;
Date.prototype.EniFormat = function () {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
    var dd = this.getDate().toString();
    return yyyy + "-" + (mm[1]?mm:"0" + mm[0]) + "-" + (dd[1]?dd:"0" + dd[0]); // padding
};

var ObjMathing = [];
ObjMathing[17078] = { id: 17078, name: "AC-Meeting", feed: { currentId: 13, voltageId: 12 } };
ObjMathing[17079] = { id: 17079, name: "AC-Main-office" , feed: { currentId: 31, voltageId: 30 } };
ObjMathing[17080] = { id: 17080, name: "AC-Tim-Ve" , feed: { currentId: 22, voltageId: 23 } };
ObjMathing[17081] = { id: 17081, name: "AC-Paula" , feed: { currentId: 16, voltageId: 17 } };
ObjMathing[17085] = { id: 17085, name: "LIGHTING" , feed: { currentId: 19, voltageId: 18 } };

var ids = [17078, 17079, 17080, 17081, 17085];
function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}
function PrvDay(id, date, days) {
    if (days == 0) {
        console.log("End " + ObjMathing[id].name);
        return;
    }
    var url = "ajax/readings?res=60&fields%5B%5D=V&fields%5B%5D=I&meter={id}&dateType=custom&from={date}&to={date}&phaseType=1";
    url = url.replace("{id}", id);
    var strDate = date.EniFormat();
    url = url.replace("{date}", strDate).replace("{date}", strDate); //ex format 2016-01-07
    //Set
    client.headers['Cookie'] = 'upgradeNotice=1;token=2be6ba4896000dd3fe3541f05974e874;justLoggedIn=1';//Back laster f
    client.get(url, function (err, res, body) {
        
        var json = body;
        for (var i = 0; i < json.data.length; i++) {
            var current = 0, voltage = 0;
            var UNXITIME;
            for (key in json.data[i]) {
                var last = key[key.length - 1]
                switch (last) {
                    case "I":
                        current = json.data[i][key];
                        break;
                    case "V":
                        voltage = json.data[i][key];
                        break;
                    case "e":
                        UNXITIME = new Date(json.data[i][key]).getTime() / 1000;
                        break;
                    default:
                        break;
                }
            }
            //http://localhost/BaseAPI/input/post.json?node=AC-Tim-Ve&json={voltage:0.1,current:0.5}&apikey=f5073713d9d0c682b647d216c53e90ec
            //var currentUrl = "http://localhost/BaseAPI/feed/insert.json?id={id}&time={UNIXTIME}&value={value}";
            //currentUrl = currentUrl.replace("{id}", ObjMathing[id].feed.currentId).replac
        }
            
    });
}

function RecordaDay(obj, objData) {
    for (var i = 0; i < objData.data.length; i++) {
        var current = 0, voltage = 0;
        var UNXITIME;
        for (key in objData.data[i]) {
            var last = key[key.length - 1]
            switch (last) {
                case "I":
                    current = objData.data[i][key];
                    break;
                case "V":
                    voltage = objData.data[i][key];
                    break;
                case "e":
                    UNXITIME = new Date(objData.data[i][key]).getTime() / 1000;
                    break;
                default:
                    break;
            }
        }
        
        var currentUrl = "/feed/insert.json?id={id}&time={UNIXTIME}&value={value}&apikey=204be581ffbc8e34af79bf25b2d638fb";
        var voltageUrl = "/feed/insert.json?id={id}&time={UNIXTIME}&value={value}&apikey=204be581ffbc8e34af79bf25b2d638fb";
        
        currentUrl = currentUrl.replace("{UNIXTIME}", UNXITIME).replace("{id}", obj.feed.currentId).replace("{value}", current); //ex format 2016-01-07
        voltageUrl = voltageUrl.replace("{UNIXTIME}", UNXITIME).replace("{id}", obj.feed.voltageId).replace("{value}", voltage);
        var headers = {"Accept-Encoding":"gzip, deflate",
                       "Host":"localhost",
                       "Accept-Encoding":"gzip, deflate",
                       "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"};
        var requestCurrent = httpSync.request({
            method: 'GET',
            headers: headers,
            body: '',               
            protocol: 'http',
            host: 'ecodefapp.dev',
            port: 80, //443 if protocol = https 
            path: currentUrl
        });
        
        var timedout = false;
        requestCurrent.setTimeout(60000, function () {
            console.log("Request Timedout!");
            timedout = true;
        });
        var responseCurrent = requestCurrent.end();
        console.log(responseCurrent.body.toString());
        
        var requestVoltage = httpSync.request({
            method: 'GET',
            headers:headers,
            body: '',               
            protocol: 'http',
            host: 'ecodefapp.dev',
            port: 80, //443 if protocol = https 
            path: voltageUrl
        });

        var timedout = false;
        requestVoltage.setTimeout(60000, function () {
            console.log("Request Timedout!");
            timedout = true;
        });
        var responseVoltage = requestVoltage.end();
        console.log(responseVoltage.body.toString());
    }
}

function main() {
    var day = 30;
    for (var i = 0; i < day; i++) {
        var date = new Date();
        date.setDate(date.getDate() - i);
        var strDate = date.EniFormat();
        for (var j = 0; j < ids.length ; j++) {
            
            var url = "/ajax/readings?res=60&fields%5B%5D=V&fields%5B%5D=I&meter={id}&dateType=custom&from={date}&to={date}&phaseType=1";
            url = url.replace("{id}", ids[i]);
            url = url.replace("{date}", strDate).replace("{date}", strDate); //ex format 2016-01-07
            var request = httpSync.request({
                method: 'GET',
                headers: { 'Cookie': 'upgradeNotice=1;token=d81ac04639981940d3fde110133e5600;justLoggedIn=1' },
                body: '',               
                protocol: 'http',
                host: 'eniscope.com',
                port: 80, //443 if protocol = https 
                path: url
            });
            
            var timedout = false;
            request.setTimeout(60000, function () {
                console.log("Request Timedout!");
                timedout = true;
            });
            var response = request.end();
            console.log(response);
            var objData = JSON.parse(response.body.toString());
            RecordaDay(ObjMathing[ids[i]], objData);
        }
    }
    console.log("---END---");
}
function main(id) {

        var day = 30;
    for (var i = 0; i < day; i++) {
        var date = new Date();
        date.setDate(date.getDate() - i);
        var strDate = date.EniFormat();
    var url = "/ajax/readings?res=60&fields%5B%5D=V&fields%5B%5D=I&meter={id}&dateType=custom&from={date}&to={date}&phaseType=1";
    url = url.replace("{id}", id);
    url = url.replace("{date}", strDate).replace("{date}", strDate); //ex format 2016-01-07
    var request = httpSync.request({
        method: 'GET',
        headers: { 'Cookie': 'upgradeNotice=1;token=d81ac04639981940d3fde110133e5600;justLoggedIn=1' },
        body: '',               
        protocol: 'http',
        host: 'eniscope.com',
        port: 80, //443 if protocol = https 
        path: url
    });
    
    var timedout = false;
    request.setTimeout(60000, function () {
        console.log("Request Timedout!");
        timedout = true;
    });
    var response = request.end();
    console.log(response);
    var objData = JSON.parse(response.body.toString());
    RecordaDay(ObjMathing[id], objData);
    }
}
main(17079);
//main();